package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
)

func main() {
	listen := flag.String("listen", ":80", "listen address in host:port form")
	flag.Parse()

	backendServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Foo", "foo")
		fmt.Fprintln(w, "this call was relayed by the reverse proxy")
	}))
	defer backendServer.Close()

	rpURL, err := url.Parse(backendServer.URL)
	if err != nil {
		log.Fatal(err)
	}
	rp := httputil.NewSingleHostReverseProxy(rpURL)
	rp.Transport = &Transport{transport: http.DefaultTransport}

	srv := http.Server{
		Addr:    *listen,
		Handler: rp,
	}
	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		// We received an interrupt signal, shut down.
		if err := srv.Shutdown(context.Background()); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// Error starting or closing listener:
		log.Printf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
}

type Transport struct {
	transport http.RoundTripper
}

func (t *Transport) RoundTrip(req *http.Request) (resp *http.Response, err error) {
	const filename = "cache.txt"
	file, err := os.Open(filename)
	if os.IsNotExist(err) {
		file, err = os.Create(filename)
		if err != nil {
			return
		}

		resp, err = t.transport.RoundTrip(req)
		if err != nil {
			return
		}
		var dump []byte
		dump, err = httputil.DumpResponse(resp, false)
		if err != nil {
			return
		}
		_, err = file.Write(dump)
		if err != nil {
			return
		}

		resp.Body = newTeeReadCloser(resp.Body, file)
		return
	} else if err != nil {
		return
	}

	br := bufio.NewReader(file)
	resp, err = http.ReadResponse(br, req)
	if err != nil {
		return
	}
	resp.Header.Add("X-Cache", "HIT")

	rangeVal := req.Header.Get("Range")
	if rangeVal == "" {
		resp.Body = struct {
			io.Reader
			io.Closer
		}{
			resp.Body,
			file,
		}
		return
	}

	var fileInfo os.FileInfo
	fileInfo, err = file.Stat()
	if err != nil {
		return
	}
	modTime := fileInfo.ModTime()

	var offset int64
	offset, err = file.Seek(int64(-br.Buffered()), io.SeekCurrent)
	if err != nil {
		return
	}

	wroteHeader := make(chan struct{})
	w := newResponserWriter(resp, wroteHeader)
	responseWriterFunc := func() {
		http.ServeContent(w, req, "", modTime, NewOffsetReadSeeker(file, offset))
		err = w.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "responseWriterFunc w.Close failed; %v", err)
		}
		err = file.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "responseWriterFunc file.Close failed; %v", err)
		}
	}
	go responseWriterFunc()
	<-wroteHeader
	return
}

type teeReadCloser struct {
	io.Reader
	wc io.WriteCloser
	bw *bufio.Writer
}

func newTeeReadCloser(r io.Reader, wc io.WriteCloser) *teeReadCloser {
	bw := bufio.NewWriter(wc)
	return &teeReadCloser{
		Reader: io.TeeReader(r, bw),
		wc:     wc,
		bw:     bw,
	}
}

func (r *teeReadCloser) Close() error {
	err := r.bw.Flush()
	if err != nil {
		return fmt.Errorf("failed to flush bufio writer in teeReadCloser; %v", err)
	}
	err = r.wc.Close()
	if err != nil {
		return fmt.Errorf("failed to close writer in teeReadCloser; %v", err)
	}
	return nil
}

type responseWriter struct {
	resp        *http.Response
	wroteHeader chan struct{}
	pw          *io.PipeWriter
}

func newResponserWriter(resp *http.Response, wroteHeader chan struct{}) *responseWriter {
	pr, pw := io.Pipe()
	resp.Body = pr
	return &responseWriter{
		resp:        resp,
		wroteHeader: wroteHeader,
		pw:          pw,
	}
}

func (r *responseWriter) Header() http.Header {
	return r.resp.Header
}

func (r *responseWriter) Write(b []byte) (n int, err error) {
	return r.pw.Write(b)
}

func (r *responseWriter) WriteHeader(statusCode int) {
	r.resp.StatusCode = statusCode
	r.resp.Status = http.StatusText(statusCode)
	close(r.wroteHeader)
}

func (r *responseWriter) Close() error {
	return r.pw.Close()
}
