package main

import (
	"io"
)

// OffsetReadSeeker is a file with the specified seek offset.
type OffsetReadSeeker struct {
	io.ReadSeeker
	offset int64
}

// NewOffsetReadSeeker creates an OffsetReadSeeker.
func NewOffsetReadSeeker(r io.ReadSeeker, offset int64) *OffsetReadSeeker {
	return &OffsetReadSeeker{
		ReadSeeker: r,
		offset:     offset,
	}
}

// Seek sets the offset on file.
// See https://golang.org/pkg/os/#File.Seek for details.
func (r *OffsetReadSeeker) Seek(offset int64, whence int) (ret int64, err error) {
	if whence == io.SeekStart {
		offset += r.offset
	}
	ret, err = r.ReadSeeker.Seek(offset, whence)
	ret -= r.offset
	return
}
